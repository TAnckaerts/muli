﻿using Muli.Business.Bands;
using Muli.Business.Models;
using System.Collections.Generic;
using Xbehave;
using Xunit;

namespace Muli.Tests.Bands
{
    public class BandServiceTests
    {
        [Scenario]
        public void CreateBand_BandShouldBeAddedToTheList(string name, List<Band> newBandList, IBandService bandService, Band newBand)
        {
            "Given the band Tankard"
                .x(() => name = "Tankard");

            "And an empty list of bands"
                .x(() => newBandList = new List<Band>());

            "And a bandService"
                .x(() => bandService = new BandService());

            "When I add the band to the list"
                .x(() => newBand = bandService.CreateBand(name));

            "Then the band is created"
                .x(() => Assert.Equal(newBand.Name, name));
        }
    }
}

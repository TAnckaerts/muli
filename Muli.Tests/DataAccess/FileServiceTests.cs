﻿using Muli.Business.Models;
using Muli.DataAccess;
using System;
using System.Collections.Generic;
using Xunit;

namespace Muli.Tests.DataAccess
{
    public class FileServiceTests
    {
        private readonly IFIleService _fileService;
        private List<User> users;

        public FileServiceTests()
        {
            _fileService = new FileService();
            users = new List<User>();
        }

        [Fact]
        public void AddUserToUserList_UserShouldBeAdded()
        {
            //arrange
            var newUser = new User
            {
                Name = "Stijn",
                Password = "MaBoiInfra",
                IsAdmin = true
            };

            //act
            _fileService.AddUserToUsers(users, newUser);

            //assert
            Assert.True(users.Count == 1);
            Assert.Contains(newUser, users);
        }

        [Theory]
        [InlineData("Pieter", "", false)]
        [InlineData("", "bloempot", false)]
        public void AddUserToUsersList_ShouldReturnAnError(string name, string password, bool isAdmin)
        {
            //arrange
            var newUser = new User
            {
                Name = name,
                Password = password,
                IsAdmin = isAdmin
            };

            //act && assert
            Assert.Throws<NullReferenceException>(() => _fileService.AddUserToUsers(users, newUser));
        }

        [Fact]
        public void GetAllUsers_ShouldReturnAllExistingUsers()
        {
            //arrange
            users.Clear();

            //act
            users = _fileService.GetAllUsers();

            //assert
            Assert.True(users.Count == 4);
        }
    }
}

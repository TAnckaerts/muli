﻿using Muli.Business.Models;
using Muli.Business.Songs;
using Muli.Business.Songs.Exceptions;
using Xunit;

namespace Muli.Tests.Songs
{
    public class SongServiceTests
    {
        private readonly ISongService _songService;

        public SongServiceTests() => _songService = new SongService();

        [Theory]
        [InlineData("Fooled by your guts", 3.15, "Fooled by your guts")]
        [InlineData("First kill", 4.22, "First kill")]
        public void Create_UserShouldBeAddedToTheList(string name, double duration, string expected)
        {
            //arrange

            //act
            var actual = _songService.CreateSong(name, duration);

            //assert
            Assert.Equal(expected, actual.Name);
        }

        [Theory]
        [InlineData("Ten thousand ways to die", "Ten thousand ways to die")]
        [InlineData("Hammer", "Hammer")]
        public void Get_ExistingUserShouldBeReturned(string name, string expected)
        {
            //arrange
            _songService.CreateSong("Ten thousand ways to die", 3.20);
            _songService.CreateSong("Hammer", 2.30);

            //act
            var actual = _songService.GetSong(name);

            //assert
            Assert.Equal(expected, actual.Name);
        }

        [Fact]
        public void Get_NonExistingUserShouldReturnAnError()
        {
            //arrange

            //act && assert
            Assert.Throws<SongNotFoundException>(() => _songService.GetSong("Never gonna give you up"));
        }

        [Fact]
        public void Update_ExistingUserShouldBeUpdated()
        {
            //arrange
            _songService.CreateSong("Through eyes of glass", 5.10);
            var expected = 5.10;

            //act
            var actual = _songService.UpdateSong("Through eyes of glass", 5.12);

            //assert
            Assert.NotEqual(expected, actual.Duration);
        }

        [Fact]
        public void Update_NonExistingUserShouldReturnAnError()
        {
            //arrange

            //act && assert
            Assert.Throws<SongNotFoundException>(() => _songService.UpdateSong("Cry me a river", 2.20));
        }

        [Theory]
        [InlineData("Violence", null)]
        [InlineData("Wanderer", null)]
        [InlineData("One more magic potion", null)]
        public void Delete_ExistingUserShouldBeDeleted(string name, Song expected)
        {
            //arrange
            _songService.CreateSong(name, 2.10);

            //act
            _songService.DeleteSong(name);
            var actual = _songService.GetSong(name);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Delete_NonExistingUserShouldReturnAnError()
        {
            //arrange

            //act && assert
            Assert.Throws<SongNotFoundException>(() => _songService.DeleteSong("Candy shop"));
        }
    }
}

﻿using Muli.Business.Login;
using Muli.Business.Models;
using Xunit;

namespace Muli.Tests
{
    public class LoginValidatorTests
    {
        private readonly ILoginService _loginValidator;

        public LoginValidatorTests() => _loginValidator = new LoginService();

        [Fact]
        private void Login_ShouldFailWithNonExistingUser()
        {
            //arrange
            var user = new User
            {
                Name = "tom",
                Password = "password"
            };
            var expected = false;

            //act
            var actual = _loginValidator.ValidateCredentials(user.Name, user.Password);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        private void Login_ShouldSucceedWithExistingUser()
        {
            //arrange
            var user = new User
            {
                Name = "Dummy",
                Password = "dummy123"
            };
            var expected = true;

            //act
            var actual = _loginValidator.ValidateCredentials(user.Name, user.Password);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}

﻿using Moq;
using Muli.Business.Models;
using Muli.Business.Users;
using Muli.Business.Users.Validators;
using Xunit;

namespace Muli.Tests.Users
{
    public class UserServiceTest
    {
        private readonly IUserService _userService;

        public UserServiceTest()
        {
            //var mockDataAccess = new Mock<IValidator>();
            //mockDataAccess.Setup(m => m.NameIsValidAndDoesNotExist()).Returns(true);
            //_userService = new UserService(mockDataAccess.Object);
            _userService = new UserService();
        }

        [Theory]
        [InlineData("Lode", "DependencyInjectionRules", "Lode")]
        [InlineData("Hannes", "C#Rules", "Hannes")]
        public void Create_UserShouldBeAddedToTheList(string name, string password, string expected)
        {
            //arrange 

            //act
            var actual = _userService.CreateUser(name, password, false);

            //assert
            Assert.Equal(expected, actual.Name);

        }

        [Theory]
        [InlineData("Joey", "Joey")]
        [InlineData("Marlies", "Marlies")]
        public void Get_ExistingUserShouldBeReturned(string name, string expected)
        {
            //arrange
            _userService.CreateUser("Joey", "dummy", false);
            _userService.CreateUser("Marlies", "dummy", false);

            //act
            var actual = _userService.GetUser(name);

            //assert
            Assert.Equal(expected, actual.Name);
        }

        [Fact]
        public void Get_NonExistingUserShouldReturnAnError()
        {
            //arrange

            //act && assert
            Assert.Throws<UserNotFoundException>(() => _userService.GetUser("John Doe"));
        }

        [Fact]
        public void Update_ExistingUserShouldBeUpdated()
        {
            //arrange
            _userService.CreateUser("Robbe", "AgileRules", false);
            var expected = "AgileRules";

            //act
            var actual = _userService.UpdateUser("Robbe", "NewPassword", false);

            //assert
            Assert.NotEqual(expected, actual.Password);
        }

        [Fact]
        public void Update_NonExistingUserShouldReturnAnError()
        {
            //arrange

            //act && assert
            Assert.Throws<UserNotFoundException>(() => _userService.UpdateUser("John Doe", "dummy123", false));
        }

        [Theory]
        [InlineData("Niels", null)]
        [InlineData("Dummy", null)]
        [InlineData("Sam", null)]
        public void Delete_ExistingUserShouldBeDeleted(string name, User expected)
        {
            //arrange
            _userService.CreateUser(name, "123wachtwoord", false);

            //act
            _userService.DeleteUser(name);
            var actual = _userService.GetUser(name);

            //assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("John")]
        [InlineData("Johny")]
        [InlineData("Jon")]
        public void Delete_NonExistingUserShouldReturnAnError(string name)
        {
            //arrange

            //act && assert
            Assert.Throws<UserNotFoundException>(

                () => _userService.DeleteUser(name));
        }
    }
}

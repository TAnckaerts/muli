﻿using System.Collections.Generic;

namespace Muli.Business.Models
{
    public class Playlist
    {
        public string Name { get; set; }
        public List<Song> songs { get; set; }
    }
}

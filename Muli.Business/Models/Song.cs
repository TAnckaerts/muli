﻿namespace Muli.Business.Models
{
    public class Song
    {
        public string Name { get; set; }
        public double Duration { get; set; }
    }
}

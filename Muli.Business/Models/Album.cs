﻿using System.Collections.Generic;

namespace Muli.Business.Models
{
    public class Album
    {
        public string Name { get; set; }
        public Band Band { get; set; }
        public List<Song> Songs { get; set; }
    }
}

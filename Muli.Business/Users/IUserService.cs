﻿using Muli.Business.Models;

namespace Muli.Business.Users
{
    public interface IUserService
    {
        User CreateUser(string name, string password, bool isAdmin);
        User GetUser(string name);
        User UpdateUser(string name, string password, bool IsAdmin);
        void DeleteUser(string name);
    }
}

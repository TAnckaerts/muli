﻿using Muli.Business.Models;
using Muli.Business.Users.Validators;
using System.Collections.Generic;
using System.Linq;

namespace Muli.Business.Users
{
    public class UserService : IUserService
    {
        private readonly List<User> _users;
        //private readonly IValidator _userNameValidator;

        public UserService(/*IValidator validator*/)
        {
            _users = new List<User>();
            //_userNameValidator = validator;
        }

        public User CreateUser(string name, string password, bool isAdmin)
        {
            //_userNameValidator.NameIsValidAndDoesNotExist();
            var newUser = new User
            {
                Name = name,
                Password = password,
                IsAdmin = isAdmin
            };

            _users.Add(newUser);
            return newUser;
        }

        public void DeleteUser(string name)
        {
            var user = GetUser(name);
            if (user != null)
            {
                _users.Remove(GetUser(name));
            }
            else
            {
                throw new UserNotFoundException("This user does not exist");
            }
        }

        public User GetUser(string name)
        {
            var user = _users.FirstOrDefault(u => u.Name.Equals(name));
            if (user != null)
            {
                return user;
            }
            else
            {
                throw new UserNotFoundException("This user does not exist");
            }
        }

        public User UpdateUser(string name, string password, bool IsAdmin)
        {
            var user = GetUser(name);
            if (user != null)
            {
                user.Name = name;
                user.Password = password;
                user.IsAdmin = IsAdmin;
                return user;
            }
            else
            {
                throw new UserNotFoundException("This user does not exist");
            }
        }
    }
}

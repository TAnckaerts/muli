﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muli.Business.Users.Validators
{
    public interface IValidator
    {
        bool NameIsValidAndDoesNotExist();
    }
}

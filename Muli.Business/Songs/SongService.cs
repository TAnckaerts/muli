﻿using Muli.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Muli.Business.Songs
{
    public class SongService : ISongService
    {
        private readonly List<Song> _songs;

        public SongService() => _songs = new List<Song>();

        public Song CreateSong(string name, double duration)
        {
            var song = new Song
            {
                Name = name,
                Duration = duration
            };
            return null;
        }

        public Song GetSong(string name) => _songs.FirstOrDefault();


        public Song UpdateSong(string name, double duration)
        {
            var song = _songs.FirstOrDefault(s => s.Duration == duration);
            song.Name = name;
            song.Duration = duration;

            return song;
        }

        public void DeleteSong(string name) => throw new NotImplementedException();
    }
}

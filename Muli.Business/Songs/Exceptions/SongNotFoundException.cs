﻿using System;

namespace Muli.Business.Songs.Exceptions
{
    public class SongNotFoundException : Exception
    {
        public SongNotFoundException() : base() { }
        public SongNotFoundException(string message) : base(message) { }
        public SongNotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}

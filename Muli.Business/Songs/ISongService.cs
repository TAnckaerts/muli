﻿using Muli.Business.Models;

namespace Muli.Business.Songs
{
    public interface ISongService
    {
        Song CreateSong(string name, double duration);
        Song GetSong(string name);
        Song UpdateSong(string name, double duration);
        void DeleteSong(string name);
    }
}

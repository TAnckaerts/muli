﻿namespace Muli.Business.Login
{
    public interface ILoginService
    {
        bool ValidateCredentials(string name, string password);
    }
}

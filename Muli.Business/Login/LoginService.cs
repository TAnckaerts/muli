﻿using Muli.Business.Models;
using System.Collections.Generic;
using System.Linq;

namespace Muli.Business.Login
{
    public class LoginService : ILoginService
    {
        private readonly List<User> _users;

        public LoginService()
        {
            _users = new List<User>();
            SetupUsers();
        }

        public bool ValidateCredentials(string name, string password)
        {
            var user = _users.FirstOrDefault(u => u.Name.Equals(name) && u.Password.Equals(password));
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetupUsers()
        {
            _users.Add(new User
            {
                Name = "Dummy",
                Password = "dummy123"
            });
        }
    }
}

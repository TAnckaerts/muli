﻿using Muli.Business.Models;
using System.Collections.Generic;

namespace Muli.Business.Playlists
{
    public interface IPlayListService
    {
        Playlist CreatePlaylist(string name, List<Song> songs);
        Playlist GetPlaylist(string name);
        Playlist UpdatePlaylist(string name, List<Song> songs);
        void DeletePlaylist(string name);
        void SortBySongName(Playlist playlist);
        void SortByBand(Playlist playlist);
    }
}

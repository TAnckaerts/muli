﻿using Muli.Business.Models;
using System;
using System.Collections.Generic;

namespace Muli.Business.Playlists
{
    public class PlayListService : IPlayListService
    {
        private readonly List<Playlist> _playLists;

        public PlayListService() => _playLists = new List<Playlist>();

        public Playlist CreatePlaylist(string name, List<Song> songs) => throw new NotImplementedException();

        public void DeletePlaylist(string name) => throw new NotImplementedException();

        public Playlist GetPlaylist(string name) => throw new NotImplementedException();

        public void SortByBand(Playlist playlist) => throw new NotImplementedException();

        public void SortBySongName(Playlist playlist) => throw new NotImplementedException();

        public Playlist UpdatePlaylist(string name, List<Song> songs) => throw new NotImplementedException();
    }
}

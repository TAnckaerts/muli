﻿using Muli.Business.Models;
using System.Collections.Generic;
using System.Linq;

namespace Muli.Business.Bands
{
    public class BandService : IBandService
    {
        private readonly List<Band> _bands;

        public BandService() => _bands = new List<Band>();

        public Band CreateBand(string name)
        {
            var newBand = new Band
            {
                Name = name
            };

            _bands.Add(newBand);
            return newBand;
        }

        public Band GetBand(string name) => _bands.FirstOrDefault(b => b.Name.Equals(name));

        public List<Band> GetAllBands() => _bands;

        public void DeleteBand(string name)
        {
            var band = GetBand(name);
            _bands.Remove(band);
        }
    }
}

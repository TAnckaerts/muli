﻿using Muli.Business.Models;
using System.Collections.Generic;

namespace Muli.Business.Bands
{
    public interface IBandService
    {
        Band CreateBand(string name);
        Band GetBand(string name);
        List<Band> GetAllBands();
        void DeleteBand(string name);
    }
}

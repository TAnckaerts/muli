﻿using Muli.Business.Models;
using System.Collections.Generic;

namespace Muli.DataAccess
{
    public interface IFIleService
    {
        void AddNewUser(User user);
        void AddUserToUsers(List<User> users, User user);
        List<string> ConvertModelsToCSV(List<User> users);
        List<User> GetAllUsers();
    }
}

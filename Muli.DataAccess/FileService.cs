﻿using Muli.Business.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace Muli.DataAccess
{
    public class FileService : IFIleService
    {
        private static readonly string usersFilePath = "UsersFile.txt";

        public FileService()
        {
        }

        public void AddNewUser(User user)
        {
            var users = GetAllUsers();

            AddUserToUsers(users, user);
            ConvertModelsToCSV(users);
            throw new NotImplementedException();
        }

        public void AddUserToUsers(List<User> users, User user)
        {
            if (string.IsNullOrWhiteSpace(user.Name) || string.IsNullOrWhiteSpace(user.Password))
            {
                throw new NullReferenceException();
            }
            else
            {
                users.Add(user);
            }
        }

        public List<string> ConvertModelsToCSV(List<User> users)
        {
            var output = new List<string>();

            foreach (var user in users)
            {
                output.Add($"{user.Name}, {user.Password}, {user.IsAdmin}");
            }

            return output;
        }

        public List<User> GetAllUsers()
        {
            var output = new List<User>();
            var content = File.ReadAllLines(usersFilePath);

            foreach (var line in content)
            {
                var data = line.Split(',');
                output.Add(
                    new User
                    {
                        Name = data[0],
                        Password = data[1],
                        IsAdmin = bool.Parse(data[2])
                    });
            }

            return output;
        }
    }
}
